console.groupCollapsed("Call, Bind, Apply");
const pets = {
    eats: true,
    breath: true,
    name: "Jhon",
    play(toy){ console.log(`${this.name} likes ${toy}.`)},
    say(word){ console.log(`${this.name} say ${word}.`)}
};

const cat = {
    name: "Mikaelle"
};

pets.play.bind(cat, "ball")();
pets.say.call(cat, "meow");
pets.say.apply(cat, ["meow"]);
console.groupEnd();
