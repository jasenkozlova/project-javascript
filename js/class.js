console.groupCollapsed("Class");

const person = Object.create(
    {
        calcAge () {
            console.log("Age", new Date().getFullYear() - this.birth);
        }
    },
    {
        name: {
            value: "Yana",
            enumerable: true,
            writable: true,
            configurable: true
        },
        birth: {
           value: '1989' ,
            enumerable: false,
            writable: false,
            configurable: false
        },
         age: {
            get() {
               return new Date().getFullYear() - this.birth
            },
             set(value) {
                console.log("Set Age", value);
             }
         }
    }
    )

person.name = "Yana Kozlova"

for (let key in person) {
    console.log("Key", key, person[key])
}

console.groupEnd();
