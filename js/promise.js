
const p = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("Promise lesson: prepearing data...");
        const backendData = {
            server: "aws",
            port: 2000,
        }
        resolve(backendData);
    }, 10000)
})

p
    .then(data => {
        return new Promise((resolve, reject) => {
        setTimeout(() => {
            data.modified = true;
            resolve(data)
        }, 10000)
        })
    })
    .then((clientData)=> {
    console.log("Promise lesson: data received", clientData);
})
    .catch(error => console.log("Promise lesson: error: ", error))
    .finally(() => console.log("Promise lesson: finally"))


const sleep = ms => {
    return new Promise(res => {
        setTimeout(() => res(), ms)
        })
    }

sleep(12000).then(() => console.log("Promise lesson: after 2 sec"));
sleep(13000).then(() => console.log("Promise lesson: after 3 sec"));

Promise.all([sleep(12000), sleep(15000)])
    .then(() => {
        console.log("Promise lesson: all promises");
    })

Promise.race([sleep(12000), sleep(15000)])
    .then(() => {
        console.log("Promise lesson: race promises");
    })

