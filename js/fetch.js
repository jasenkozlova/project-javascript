const URLFetch = 'https://jsonplaceholder.typicode.com/users'

function sendRequest (method, url, body) {
    return fetch(url, {
        method,
        body: JSON.stringify(body),
        headers: {'Content-type': 'application/json'}
    })
        .then(res => {
            if (res.ok) return res.json()
            return  res.json().then(err => {
                const e = new Error('Something went wrong')
                e.data = err
                throw e
            })
        })
}

sendRequest("GET", URLFetch)
    .then(data => console.log(data))
    .catch(err => console.log(err))

const body = {
    name: "Yana"
}
sendRequest("POST", URLFetch, body)
    .then(data => console.log(data))
    .catch(err => console.log(err))
