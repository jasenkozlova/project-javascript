const set = new Set([1,2,3,4,5,6,7,8,9,10])

set.add(11).add(20).add(20)
console.log(set)
console.log(set.has(42))
console.log(set.size)
console.log(set.delete(9))
console.log(set.size)

console.log(set.values())

for (let key of set) {
    console.log(key)
}

function uniqValues(array) {
    return [...new Set(array)]
}

console.log(uniqValues([1,1,2,2,3,3,4,4]))


