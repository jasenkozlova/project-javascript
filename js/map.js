console.groupCollapsed("Map, Set, WeakMap, WeakSet")

const entries = [
    ['name', 'Yana'],
    ['job', 'frontend']
]

const map = new Map(entries)
map.set('newField', 2)
    .set(NaN, "value of NaN")

map.delete("job")

console.log(map)
console.log(map.has("job"))
console.log(map.size)
// map.clear()
// console.log(map)

for (let [key, value] of map.entries()) {
    console.log(key, value)
}

for (let val of map.values()) {
    console.log(val)
}

for (let key of map.keys()) {
    console.log(key)
}

map.forEach((key, value) => {
    console.log(key, value)
})

const array = [...map]
console.log(array)

const mapObj = Object.fromEntries(map.entries())
console.log(mapObj)


console.groupEnd()
