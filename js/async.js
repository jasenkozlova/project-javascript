const delay = ms => {
    return new Promise(res => setTimeout(() =>
        res(), ms
    ))
}

const URL = "https://jsonplaceholder.typicode.com/todos/12";

//  function fetchTodos () {
//      console.log("fetchTodos started")
//      return delay(2000)
//          .then(() => fetch(URL))
//          .then(res => res.json())
//  }
//
// fetchTodos()
//     .then(data => {
//         console.log(data)
//     })
//     .catch(error => console.log(error))


async function fetchAsyncTodos () {
    console.log("Async/await lesson: fetchTodos started");
    try {
        await delay(16000);
        const responce = await fetch(URL);
        const data = await responce.json();
        console.log("Async/await lesson: data: " , data);
    } catch (e) {
        console.log(e);
    } finally {
        console.log("Async/await lesson: finally");
    }

}

fetchAsyncTodos()
