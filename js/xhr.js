// const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const URLFetch = 'https://jsonplaceholder.typicode.com/users'

function sendRequest (method, url, body) {
    return new Promise((resolve, reject)=> {
        const xhr = new XMLHttpRequest()
        xhr.open(method, url)
        xhr.responseType = "json"
        xhr.setRequestHeader("Content-type", 'application/json')

        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = () => {
            reject(xhr.responseText)
        }
        xhr.send(body);
    })
}

sendRequest("GET", URLFetch)
    .then(data => console.log(data))
    .catch(err => console.log(err))

const body = {
    name: "Yana"
}
sendRequest("POST", URLFetch, JSON.stringify(body))
    .then(data => console.log(data))
    .catch(err => console.log(err))
