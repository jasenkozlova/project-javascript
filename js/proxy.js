console.groupCollapsed("Proxy");
const personProxy = {
    name: "yana",
    age: "31",
    job: "front-end developer"
}

const op = new Proxy(personProxy, {
    get(target, prop) {
        console.log(`getting prop ${prop}`)
        return target[prop]
    },
    set(target, prop, value) {
       if (prop in target) {
           target[prop] = value
       } else {
           throw new Error(`no ${prop} field in target`)
       }
    },
    has(target, prop) {
        return ["age", "name", "job"].includes(prop)
    },
    deleteProperty(target, prop){
        console.log("deleting...", prop)
        delete target[prop]
        return true
    }
})

console.log(op.age = 26)

const log = text => console.log(`Log: ${text}`)

const fp = new Proxy(log, {
    apply(target, thisArg, args) {
        console.log("Calling fn...")
        return target.apply(thisArg, args)
    }
})

fp("text");

class Person {
    constructor(name, age) {
        this.name = name
        this.age = age
    }
}

const PersonProxy = new Proxy( Person, {
    construct(target, argArray) {
        console.log("construct...")
        return new Proxy(new target(...argArray), {
            get(t, prop) {
                console.log(`Getting prop ${prop}`)
                return t[prop]
            }
        })
    }
})

console.groupEnd();
