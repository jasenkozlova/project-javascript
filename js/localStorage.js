console.groupCollapsed("Local storage")

const object = {
    name: 'Yana',
    age: 31,
    id: 2773468343,
};

localStorage.setItem("key_name", JSON.stringify(object));
const arr = JSON.parse(localStorage.getItem('key_name')) || [];
console.log(arr);

console.groupEnd()
