console.groupCollapsed("Object");
class Components {
    constructor(selector) {
        this.$el = document.querySelector(selector)
    }

    hide () {
        this.$el.style.display = 'none'
    }

    show () {
        this.$el.style.display = 'block'
    }
}

class Box extends Components {
    constructor(options) {
        super(options.selector);

        this.$el.style.width = this.$el.style.height = options.size + 'px'
        this.$el.style.backgroundColor = options.color
    }
}

const box1 = new Box({
    selector: "#box1",
    size: 100,
    color: "red"
})

const box2 = new Box({
    selector: "#box2",
    size: 120,
    color: "blue"
})
box1.show()
box2.show()

console.groupEnd();
