console.groupCollapsed("Closures");

// 0, 1, 1, 2, 3, 5, 8, 13

function getNextFibonacci() {
  let prev = -1;
  let curr = 1;
  return function() {
    let number = prev + curr;
    let temp = prev;
    prev = curr;
    curr = temp + curr;
    return number;
  }
}

let fibonacciNumber = getNextFibonacci();

for (i=0; i<20; i++) console.log(fibonacciNumber());

console.groupEnd();
