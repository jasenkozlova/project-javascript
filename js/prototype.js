console.groupCollapsed("Prototype");
const pet = {
    eats: true,
    breath: true,
    name: "Jhon",
    play(toy){ return `${this.name} likes ${toy}.` }
};

const dog = {
    name: "Max",
    say(){ return `${this.name} say woof`}
};

dog.__proto__ = pet;

console.log(dog.say());
console.log(dog.play("ball"));
console.log(dog.name);
console.groupEnd();
