const DATA = [
    {
        "_id": "607b81a40c40a775ce4e3081",
        "isActive": true,
        "age": 38,
        "eyeColor": "blue",
        "name": "Monique Roy",
        "gender": "female",
        "company": "EPLOSION",
        "email": "moniqueroy@eplosion.com",
        "phone": "+1 (866) 423-3063",
        "address": "875 Meeker Avenue, Sedley, Ohio, 2448",
        "greeting": "Hello, Monique Roy! You have 10 unread messages.",
        "favoriteFruit": "apple"
    },
    {
        "_id": "607b81a4cdf4f94f2afb6e7b",
        "isActive": false,
        "age": 21,
        "eyeColor": "green",
        "name": "Lacey Cortez",
        "gender": "female",
        "company": "DANCITY",
        "email": "laceycortez@dancity.com",
        "phone": "+1 (810) 439-3741",
        "address": "918 Oxford Walk, Weeksville, New York, 3679",
        "greeting": "Hello, Lacey Cortez! You have 4 unread messages.",
        "favoriteFruit": "apple"
    },
    {
        "_id": "607b81a4647ef6febeb79994",
        "isActive": false,
        "age": 30,
        "eyeColor": "brown",
        "name": "Carrillo Brady",
        "gender": "male",
        "company": "GAZAK",
        "email": "carrillobrady@gazak.com",
        "phone": "+1 (978) 469-3551",
        "address": "922 Battery Avenue, Loretto, Pennsylvania, 1978",
        "greeting": "Hello, Carrillo Brady! You have 7 unread messages.",
        "favoriteFruit": "banana"
    },
    {
        "_id": "607b81a4213d694440f82845",
        "isActive": false,
        "age": 38,
        "eyeColor": "green",
        "name": "Hewitt Barrett",
        "gender": "male",
        "company": "MAXIMIND",
        "email": "hewittbarrett@maximind.com",
        "phone": "+1 (950) 488-3071",
        "address": "757 Morton Street, Talpa, Wyoming, 2462",
        "greeting": "Hello, Hewitt Barrett! You have 5 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "607b81a47abc186102fff636",
        "isActive": true,
        "age": 33,
        "eyeColor": "blue",
        "name": "Cooper Murray",
        "gender": "male",
        "company": "ETERNIS",
        "email": "coopermurray@eternis.com",
        "phone": "+1 (810) 456-3012",
        "address": "195 Navy Walk, Brandywine, Maine, 1859",
        "greeting": "Hello, Cooper Murray! You have 8 unread messages.",
        "favoriteFruit": "banana"
    },
    {
        "_id": "607b81a4ec17b7791bd76977",
        "isActive": false,
        "age": 26,
        "eyeColor": "brown",
        "name": "Camacho Rose",
        "gender": "male",
        "company": "ORBOID",
        "email": "camachorose@orboid.com",
        "phone": "+1 (946) 407-2489",
        "address": "479 Bartlett Street, Basye, Louisiana, 4169",
        "greeting": "Hello, Camacho Rose! You have 7 unread messages.",
        "favoriteFruit": "strawberry"
    }
]
console.groupCollapsed("Arrays methods");
const hasBlueEye = DATA.some(person => person.eyeColor === "blue");
console.log("hasBlueEye", hasBlueEye);

const allHasBlueEye = DATA.every(person => person.eyeColor === "blue");
console.log("allHasBlueEye", allHasBlueEye);

const avgAge = DATA.reduce((accumulator, currentValue) => accumulator + currentValue.age, 0) / DATA.length;
console.log("avgAge", avgAge);

const names = DATA.map(person => person.name);
console.log("names", names);

const isActivePerson = DATA.filter(person => person.isActive);
console.log("isActivePerson", isActivePerson);

const index = DATA.findIndex(person => person._id === "607b81a4cdf4f94f2afb6e7b")
console.log("index", index);

const sortUp = DATA.sort((curr, next) => curr.age - next.age);
console.log("sortUp", sortUp);
console.groupEnd();
