let obj = {
    "_id": "607b81a40c40a775ce4e3081",
    "isActive": true,
    "age": 38,
    "eyeColor": "blue",
    "name": "Monique Roy",
    "gender": "female",
    "company": "EPLOSION",
    "email": "moniqueroy@eplosion.com",
    "phone": "+1 (866) 423-3063",
    "address": "875 Meeker Avenue, Sedley, Ohio, 2448",
    "greeting": "Hello, Monique Roy! You have 10 unread messages.",
    "favoriteFruit": "apple"
}

console.log({...obj});

obj = {
    ...obj,
    name: "Yana Kozlova"
}

console.log(obj);

// const divs = document.querySelectorAll("div")
// const nodes = [...divs]
// console.log(nodes)


function sum(a, b, ...rest) {
    return a + b + rest.reduce((acum, item) => acum + item, 0)
}

const numbers = [1, 2, 3, 4, 5]
console.log(sum(...numbers))

const [a, b, ...other] = numbers

const {name, age} = obj
console.log(name, age)
