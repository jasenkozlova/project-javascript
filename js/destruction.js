function calcVal(a, b) {
    return [
        a + b,
        a - b,
        a * b,
        a / b
    ]
}

// const [sum, sub] = calcVal(32,52)
const [sum, , mult, ...other] = calcVal(32,52)
console.log(sum, mult, other)

let objDestruction = {
    "_id": "607b81a40c40a775ce4e3081",
    "isActive": true,
    "age": 38,
    "eyeColor": "blue",
    "name": "Monique Roy",
    "gender": "female",
    "company": "EPLOSION",
    "email": "moniqueroy@eplosion.com",
    "phone": "+1 (866) 423-3063",
    "address": "875 Meeker Avenue, Sedley, Ohio, 2448",
    "greeting": "Hello, Monique Roy! You have 10 unread messages.",
    "favoriteFruit": "apple"
}

const {
    name: personName = "John Doe",
    age,
    car = 'no car'}
    = objDestruction
console.log(personName, age, car)


