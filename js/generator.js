console.groupCollapsed("Generator");

// function strGenerator () {
//     yield 'Y'
//     yield 'a'
//     yield 'n'
//     yield 'a'
// }
//
// const nameGen = strGenerator()

// function numberGen(n= 10) {
//     for (let i=0; i<n; i++) {
//         yield i
//     }
// }
//
// const nameGen = numberGen(20)

const iterator = {
[Symbol.iterator](n=10) {
        let i = 0
        return {
            next() {
                if (i<n) {
                    return {value: ++i, done: false}
                }
                return {value: undefined, done: true}
            }
        }
    }
}

for (let key of iterator) {
    console.log(key)
}


console.groupEnd();
